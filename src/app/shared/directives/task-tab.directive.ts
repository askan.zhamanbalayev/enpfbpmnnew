import { ViewContainerRef, Directive } from '@angular/core';

@Directive({
    selector: '[active-tab]'
})
export class TaskTabDirective {
    constructor(public viewContainerRef: ViewContainerRef) {}
}