export interface ProcessDefinition {
    id: string;
    key: string;
    category: string;
    description?: any;
    name: string;
    version: number;
    resource: string;
    deploymentId: string;
    diagram?: any;
    suspended: boolean;
    tenantId?: any;
    versionTag: string;
    historyTimeToLive: number;
    startableInTasklist: boolean;
}

export interface TaskInfo {
    id: string;
    name: string;
    assignee: string;
    created: string;
    due?: any;
    followUp?: any;
    delegationState?: any;
    description?: any;
    executionId: string;
    owner?: any;
    parentTaskId?: any;
    priority: number;
    processDefinitionId: string;
    processInstanceId: string;
    taskDefinitionKey: string;
    caseExecutionId?: any;
    caseInstanceId?: any;
    caseDefinitionId?: any;
    suspended: boolean;
    formKey?: any;
    tenantId?: any;
}

export class ValueTouple {
    id: string;
    value: string;
}

export class TaskFieldInfo {
    name: string;
    defaultValue?: any;
    values?: ValueTouple[];
    type: string;    
}