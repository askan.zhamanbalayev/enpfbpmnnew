import { Component, OnInit, Output, EventEmitter, OnDestroy, forwardRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { BpmnService } from 'src/app/services/bpmn.service';
import { ProcessDefinition, TaskFieldInfo, ValueTouple } from 'src/app/shared/data/bpmn';
import { FormGroup, FormControl, Validators, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { BpmnDateAdapter, BPMN_DATE_FORMATS } from '../../shared/adapters/bpmn-data-adapter';
import * as _ from "lodash";
import { chain } from 'lodash/fp';

import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => StartProcessFormComponent),
  multi: true
};

@Component({
  selector: 'app-start-process-form',
  templateUrl: './start-process-form.component.html',
  styleUrls: ['./start-process-form.component.scss'],
  providers: [
    CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR,
    { provide: DateAdapter, useClass: BpmnDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: BPMN_DATE_FORMATS },
    {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: ['l', 'LL'],
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
  ]
})
export class StartProcessFormComponent implements OnInit, OnDestroy {

  @Output() fromStartForm = new EventEmitter<string>();
  allProcessSubscription: Subscription;
  getProcessVarsSubscription: Subscription;
  startProcessSubscription: Subscription;
  loaded: boolean = false;

  form: FormGroup;

  processDefs: ProcessDefinition[];
  selectedProcessVars: any[] = [];
  selectedProcess: any;
  fieldsList: TaskFieldInfo[];

  constructor(protected bpmnService: BpmnService, private _adapter: DateAdapter<any>) { 
    this._adapter.setLocale('ru');
  }

  ngOnInit() {
    this.form = new FormGroup({});
    this.getAllProcesses();
  }

  ngOnDestroy() {
    if (this.allProcessSubscription)
      this.allProcessSubscription.unsubscribe();

    if (this.getProcessVarsSubscription)
      this.getProcessVarsSubscription.unsubscribe();

    if (this.startProcessSubscription) {
      this.startProcessSubscription.unsubscribe();
    }
  }

  onProcessSelectionChanged(event) {
    this.getProcessVariables(event.value.id);
  }

  getAllProcesses() {
    this.loaded = true;
    this.allProcessSubscription = this.bpmnService.getAllProcessDefinitions()
      .subscribe(
        result => {
          let groupedProc = chain(result)
          .groupBy('name')
          .mapValues(a => ({
            max: _.maxBy(a, 'version')
          }
          )).value();
          
          let resultObj = [];
          Object.keys(groupedProc).forEach(key => {
            let keyValue = groupedProc[key];
            Object.keys(keyValue).forEach(subKey => {
              resultObj.push(keyValue[subKey]);
            });
          });

          this.processDefs = resultObj as ProcessDefinition[];
          this.loaded = false;
        },
        error => {
          this.loaded = false;
          console.log('getAllProcessError ', error);
        }
      );
  }

  isEmpty(val) {
    return (val === undefined || val == null || val.length <= 0) ? true : false;
  }

  nvl(val, defaultVal) {
    if (!this.isEmpty(val)) return val;
    return defaultVal;
  }

  toDateISO(dateString) {
    return this.toDate(dateString).toISOString();
  }

  toDateCamunda(dateString) {
    let dd = dateString;
    if (typeof dd === 'object') {
      dd = dd._d.toISOString();
    }
    return dd.substring(0, 10).split('-').reverse().join('/');
  }

  toDate(dateString) {
    return new Date(dateString.split('/').reverse().join('-'));
  }
 
  getProcessVariables(processDefinition) {
    this.loaded = true;
    this.form = new FormGroup({});
    this.bpmnService
      .getProcessXmlByDefinitionId(processDefinition)
      .subscribe(
        result => {
          this.loaded = false;

          let parser = new DOMParser();
          let xmlDoc = parser.parseFromString(result.bpmn20Xml, "text/xml");
          let xeStartEvent = xmlDoc.getElementsByTagName("bpmn:startEvent")[0];

          this.fieldsList = [];

          Array.from(xeStartEvent.getElementsByTagName("camunda:formField")).forEach(xeFormField => {
            let fieldInfo: TaskFieldInfo = new TaskFieldInfo();
            fieldInfo.name = this.nvl(xeFormField.getAttribute("label"), this.nvl(xeFormField.getAttribute("id"), ""));
            fieldInfo.type = this.nvl(xeFormField.getAttribute("type"), "string");
            fieldInfo.defaultValue = this.nvl(xeFormField.getAttribute("defaultValue"), "");
            fieldInfo.values = [];

            Array.from(xeFormField.getElementsByTagName("camunda:value")).forEach(xeValue => {
              let val: ValueTouple = new ValueTouple();
              val.id = this.nvl(xeValue.getAttribute("id"), "");
              val.value = this.nvl(xeValue.getAttribute("name"), "");
              fieldInfo.values.push(val);
            });
            this.fieldsList.push(fieldInfo);
            
            if (fieldInfo.type === 'date') {
              let dateVal = "";
              if (fieldInfo.defaultValue !== '') {
                dateVal = this.toDateISO(fieldInfo.defaultValue);
              }
              this.form.addControl(fieldInfo.name, new FormControl(dateVal, []));
            }
            else {
              this.form.addControl(fieldInfo.name, new FormControl(fieldInfo.defaultValue, []));
            }

            let xeValidation = xeFormField.getElementsByTagName("camunda:validation");
            let validatorsObj = [];
            if (xeValidation.length > 0) {
              Array.from(xeValidation[0].getElementsByTagName("camunda:constraint")).forEach(xeConstraint => {
                let validationVal = this.nvl(xeConstraint.getAttribute("config"), "");
                switch (xeConstraint.getAttribute("name").toUpperCase()) {
                  case "REQUIRED":
                    validatorsObj.push(Validators.required);                   
                    break;
                  case "MIN":
                    if (validationVal !== "") {
                      // if (fieldInfo.type == 'date') {
                      //   let dateVal = this.toDateISO(validationVal);
                      //   validatorsObj.push(Validators.min(dateVal));
                      // }
                      // else {
                        validatorsObj.push(Validators.min(Number(validationVal))); 
                      //}
                    }
                       
                    break;
                  case "MINLENGTH":
                    if (validationVal !== "") 
                      validatorsObj.push(Validators.minLength(Number(validationVal)));
                    break;
                  case "MAX":
                    if (validationVal !== "") {
                      // if (fieldInfo.type == 'date') {
                      //   let dateVal = this.toDateISO(validationVal);
                      //   validatorsObj.push(Validators.max(dateVal));
                      // }
                      // else {
                        validatorsObj.push(Validators.max(Number(validationVal))); 
                      //}
                    }
                    break;
                  case "MAXLENGTH":
                    if (validationVal !== "") 
                      validatorsObj.push(Validators.maxLength(Number(validationVal)));  
                    break;
                  case "READONLY":
                    this.form.controls[fieldInfo.name].disable();
                    break;
                }
              });

              if (validatorsObj.length > 0) {
                this.form.controls[fieldInfo.name].setValidators(validatorsObj);
                this.form.controls[fieldInfo.name].updateValueAndValidity();
              }
            }
          });

          //console.log(JSON.stringify(this.fieldsList));
        },
        error => {
          this.loaded = false;
          console.log('getProcessXmlError ', error);
        }
      );
  }

  sendForm() {
    this.loaded = true;
    let vars = "{";
    if (this.fieldsList.length > 0) {
      vars += "\"variables\": {";
      for (var idx = 0; idx < this.fieldsList.length; idx++) {
        vars += "\"" + this.fieldsList[idx].name + "\": {\"value\":";
        switch(this.fieldsList[idx].type.toUpperCase()) {
          case "LONG":
          case "BOOLEAN":
            vars += this.form.get(this.fieldsList[idx].name).value;
            break;
          case "DATE":
            vars += "\"" + this.toDateCamunda(this.form.get(this.fieldsList[idx].name).value) + "\""; 
            break; 
          default:
            vars += "\"" + this.form.get(this.fieldsList[idx].name).value + "\"";
            break;
        }
         vars += ", \"type\": \"" + "String"/*this.fieldsList[idx].type*/ + "\"}";

        if (idx != this.fieldsList.length - 1) vars += ", ";
      }
      vars += "}";
    }
    vars += "}";

    //console.log(vars);

    this.startProcessSubscription = this.bpmnService.startProcessById(this.selectedProcess.id, vars)
      .subscribe(
        result => {
          this.loaded = false;
          this.fromStartForm.emit('');
        },
        error => {
          console.log('startProcessError ', error);
          this.loaded = false;
        }
      );
  }

  closeForm() {
    this.fromStartForm.emit('');
  }
}
