import { Component, OnInit,  OnDestroy} from '@angular/core';
import { trigger, transition, query, style, animate, group } from '@angular/animations';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { BpmnService } from '../../services/bpmn.service';
import { Subscription } from 'rxjs';
import { DiagramComponent } from 'src/app/dialogs/diagram/diagram.component';

const left = [
  query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
  group([
    query(':enter', [style({ transform: 'translateX(-100%)' }), animate('.2s ease-out', style({ transform: 'translateX(0%)' }))], {
      optional: true,
    }),
    query(':leave', [style({ transform: 'translateX(0%)' }), animate('.2s ease-out', style({ transform: 'translateX(100%)' }))], {
      optional: true,
    }),
  ]),
];

const right = [
  query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
  group([
    query(':enter', [style({ transform: 'translateX(100%)' }), animate('.2s ease-out', style({ transform: 'translateX(0%)' }))], {
      optional: true,
    }),
    query(':leave', [style({ transform: 'translateX(0%)' }), animate('.2s ease-out', style({ transform: 'translateX(-100%)' }))], {
      optional: true,
    }),
  ]),
];

export class Group {
  level = 0;
  parent: Group;
  expanded = false;
  totalCounts = 0;
  get visible(): boolean {
    return !this.parent || (this.parent.visible && this.parent.expanded);
  }
}

@Component({
  selector: 'app-tast-list',
  templateUrl: './tast-list.component.html',
  styleUrls: ['./tast-list.component.scss'],
  animations: [
    trigger('animSlider', [
      transition(':increment', right),
      transition(':decrement', left),
    ]),
  ],
})
export class TastListComponent implements OnInit,  OnDestroy {
  
  public dataSource = new MatTableDataSource<any | Group>([]);

  _alldata: any[];
  columns: any[];
  displayedColumns: string[];
  groupByColumns: string[] = [];
  getTasksSubscription: Subscription;
  closeDialogSubscription: Subscription;
  currentPage: number = 0;
  
  currentTaskId: string = "";
  currentProcessDefinitionId: string = "";
  currentTaskDefinitionKey: string = "";

  constructor(
    protected bpmnService:BpmnService, 
    public dialog: MatDialog
  ) {
    this.columns = [     
    {
      field: 'id'
    }, {
      field: 'name'
    }, {
      field: 'created'
    }, {
      field: 'processCode'
    }, {
      field: 'processVersion'
    },
  ];
    this.displayedColumns = this.columns.map(column => column.field);        
    this.groupByColumns = ['processCode'];
  }

  loaded: boolean = false;

  ngOnInit() {
    this.getTasks();
  }

  getTasks() {
    this.loaded = true;
    this.getTasksSubscription = this.bpmnService
      .getAllTasks()
      .subscribe(
        result => {
          this.loaded = false;
          this._alldata = result
            .filter(f => f.processDefinitionId !== null)
            .map(item => {
              const procDef = item.processDefinitionId.toString()
              Object.defineProperty(item, "processCode", {
                value : procDef.substring(0, procDef.indexOf(":"))})
              Object.defineProperty(item, "processVersion", {
                value : procDef.substring(procDef.indexOf(":") + 1, procDef.lastIndexOf(":"))})
              return item
            }).sort(this.compareValues('created', 'desc'));
            this.dataSource.data = this.addGroups(this._alldata, this.groupByColumns);
            this.dataSource.filterPredicate = this.customFilterPredicate.bind(this);
            this.dataSource.filter = performance.now().toString();  
        },
        error => {
          this.loaded = false;
          console.log('getTaskError ', error)
        }
      );
  }

  ngOnDestroy() {
    if (this.getTasksSubscription) {
      this.getTasksSubscription.unsubscribe();
    }

    if (this.closeDialogSubscription) {
      this.closeDialogSubscription.unsubscribe();
    }

    
  }

  startProcess() {
    this.currentPage = 2;
  }

  onRowDoubleClick(row) {    
    if (this.currentPage == 0) {
      console.log(row);
      this.currentTaskId = row.id;
      this.currentTaskDefinitionKey = row.taskDefinitionKey
      this.currentProcessDefinitionId = row.processDefinitionId;
      this.currentPage++;
    } 
  }

  compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0;
      }
  
      const varA = (typeof a[key] === 'string')
        ? a[key].toUpperCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toUpperCase() : b[key];
  
      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order === 'desc') ? (comparison * -1) : comparison
      );
    };
  }

  getTaskDiagram(row: any, column: string) {
    if (column === 'name') {
      console.log(row);
      const dialogRef = this.dialog.open(DiagramComponent, {
        width: '900px',
        height: '700px',
        data: { 
          processDefinitionId: row.processDefinitionId, 
          taskDefinitionKey: row.taskDefinitionKey,
          taskName: row.name 
        },
        disableClose: true
      });
      this.closeDialogSubscription = dialogRef.afterClosed().subscribe(result => result);
    }
  }

  onFromTaskForm($event){
    if (this.currentPage > 0) {
      this.getTasks();
      this.currentPage--;
      // let data = this.dataSource.data;
      // let idx = data.findIndex(x => x.id === $event.toString());
      // data.splice(idx, 1);
      // this.dataSource = new MatTableDataSource<any | Group>(data);
    }
  }

  onFromStartForm($event) {
    this.getTasks();
    this.currentPage = 0;
  }

  groupBy(event, column) {
    event.stopPropagation();
    this.checkGroupByColumn(column.field, true);
    this.dataSource.data = this.addGroups(this._alldata, this.groupByColumns);
    this.dataSource.filter = performance.now().toString();
  }

  checkGroupByColumn(field, add ) {
    let found = null;
    for (const column of this.groupByColumns) {
      if (column === field) {
        found = this.groupByColumns.indexOf(column, 0);
      }
    }
    if (found != null && found >= 0) {
      if (!add) {
        this.groupByColumns.splice(found, 1);
      }
    } else {
      if ( add ) {
        this.groupByColumns.push(field);
      }
    }
  }

  unGroupBy(event, column) {
    event.stopPropagation();
    this.checkGroupByColumn(column.field, false);
    this.dataSource.data = this.addGroups(this._alldata, this.groupByColumns);
    this.dataSource.filter = performance.now().toString();
  }
  
  customFilterPredicate(data: any | Group, filter: string): boolean {
    return (data instanceof Group) ? data.visible : this.getDataRowVisible(data);
  }

  getDataRowVisible(data: any): boolean {
    const groupRows = this.dataSource.data.filter(
      row => {
        if (!(row instanceof Group)) {
          return false;
        }
        let match = true;
        this.groupByColumns.forEach(column => {
          if (!row[column] || !data[column] || row[column] !== data[column]) {
            match = false;
          }
        });
        return match;
      }
    );

    if (groupRows.length === 0) {
      return true;
    }
    const parent = groupRows[0] as Group;
    return parent.visible && parent.expanded;
  }

  groupHeaderClick(row) {
    row.expanded = !row.expanded;
    this.dataSource.filter = performance.now().toString();  // bug here need to fix
  }

  addGroups(data: any[], groupByColumns: string[]): any[] {
    const rootGroup = new Group();
    rootGroup.expanded = true;
    return this.getSublevel(data, 0, groupByColumns, rootGroup);
  }

  getSublevel(data: any[], level: number, groupByColumns: string[], parent: Group): any[] {
    if (level >= groupByColumns.length) {
      return data;
    }
    const groups = this.uniqueBy(
      data.map(
        row => {
          const result = new Group();
          result.level = level + 1;
          result.parent = parent;
          for (let i = 0; i <= level; i++) {
            result[groupByColumns[i]] = row[groupByColumns[i]];
          }
          return result;
        }
      ),
      JSON.stringify);

    const currentColumn = groupByColumns[level];
    let subGroups = [];
    groups.forEach(group => {
      const rowsInGroup = data.filter(row => group[currentColumn] === row[currentColumn]);
      group.totalCounts = rowsInGroup.length;
      const subGroup = this.getSublevel(rowsInGroup, level + 1, groupByColumns, group);
      subGroup.unshift(group);
      subGroups = subGroups.concat(subGroup);
    });
    return subGroups;
  }

  uniqueBy(a, key) {
    const seen = {};
    return a.filter((item) => {
      const k = key(item);
      return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    });
  }

  isGroup(index, item): boolean {
    return item.level;
  }  
}



