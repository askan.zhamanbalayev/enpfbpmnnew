import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  Input,
  forwardRef,
} from "@angular/core";
import { FormGroup, FormControl, Validators, NG_VALUE_ACCESSOR } from "@angular/forms";
import { TaskFieldInfo, ValueTouple } from "src/app/shared/data/bpmn";
import { BpmnService } from "src/app/services/bpmn.service";
import { Subscription } from "rxjs";

import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { BpmnDateAdapter, BPMN_DATE_FORMATS } from 'src/app/shared/adapters/bpmn-data-adapter';

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => TaskItemComponent),
  multi: true
};

@Component({
  selector: "app-task-item",
  templateUrl: "./task-item.component.html",
  styleUrls: ["./task-item.component.scss"],
  providers: [
    CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR,
    { provide: DateAdapter, useClass: BpmnDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: BPMN_DATE_FORMATS },
    {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: ['l', 'LL'],
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
  ]
})
export class TaskItemComponent implements OnInit, OnDestroy {
  @Output() fromTaskForm = new EventEmitter<string>();
  @Input() currentTaskId: string;
  @Input() currentTaskDefinitionKey: string;
  @Input() currentProcessDefinitionId: string;
  fieldsList: TaskFieldInfo[];
  form: FormGroup;
  opemTasksSubscription: Subscription;
  sendFormSubscription: Subscription;
  taskName: string = "";
  loaded: boolean = false;

  constructor(protected bpmnService: BpmnService, private _adapter: DateAdapter<any>) { 
    this._adapter.setLocale('ru');
  }

  ngOnInit() {
    this.form = new FormGroup({});
    this.initTaskForm();
  }

  ngOnDestroy() {
    if (this.opemTasksSubscription) {
      this.opemTasksSubscription.unsubscribe();
    }

    if (this.sendFormSubscription) {
      this.sendFormSubscription.unsubscribe();
    }
  }

  isEmpty(val) {
    return (val === undefined || val == null || val.length <= 0) ? true : false;
  }
  
  nvl(val, defaultVal) {
    if (!this.isEmpty(val)) return val;
    return defaultVal;
  }

  toDateISO(dateString) {
    return this.toDate(dateString).toISOString();
  }

  toDateCamunda(dateString) {
    let dd = dateString;
    if (typeof dd === 'object') {
      dd = dd._d.toISOString();
    }
    return dd.substring(0, 10).split('-').reverse().join('/');
  }

  toDate(dateString) {
    return new Date(dateString.split('/').reverse().join('-'));
  }

  initTaskForm(): void {
    this.loaded = true;
    this.form = new FormGroup({});
    this.bpmnService
      .getProcessXmlByDefinitionId(this.currentProcessDefinitionId)
      .subscribe(
        result => {
          this.loaded = false;
          this.fieldsList = [];

          let parser = new DOMParser();
          let xmlDoc = parser.parseFromString(result.bpmn20Xml, "text/xml");
          Array.from(xmlDoc.getElementsByTagName("bpmn:userTask")).forEach(xeTask => {
            if (xeTask.getAttribute('id') === this.currentTaskDefinitionKey) {
              Array.from(xeTask.getElementsByTagName("camunda:formField")).forEach(xeFormField => {
                let fieldInfo: TaskFieldInfo = new TaskFieldInfo();
                fieldInfo.name = this.nvl(xeFormField.getAttribute("label"), this.nvl(xeFormField.getAttribute("id"), ""));
                fieldInfo.type = this.nvl(xeFormField.getAttribute("type"), "string");
                fieldInfo.defaultValue = this.nvl(xeFormField.getAttribute("defaultValue"), "");
                fieldInfo.values = [];
    
                Array.from(xeFormField.getElementsByTagName("camunda:value")).forEach(xeValue => {
                  let val: ValueTouple = new ValueTouple();
                  val.id = this.nvl(xeValue.getAttribute("id"), "");
                  val.value = this.nvl(xeValue.getAttribute("name"), "");
                  fieldInfo.values.push(val);
                });
                this.fieldsList.push(fieldInfo);
                
                if (fieldInfo.type === 'date') {
                  let dateVal = "";
                  if (fieldInfo.defaultValue !== '') {
                    dateVal = this.toDateISO(fieldInfo.defaultValue);                    
                  }
                  this.form.addControl(fieldInfo.name, new FormControl(dateVal, []));
                }
                else {
                  this.form.addControl(fieldInfo.name, new FormControl(fieldInfo.defaultValue, []));
                }
    
                let xeValidation = xeFormField.getElementsByTagName("camunda:validation");
                let validatorsObj = [];
                if (xeValidation.length > 0) {
                  Array.from(xeValidation[0].getElementsByTagName("camunda:constraint")).forEach(xeConstraint => {
                    let validationVal = this.nvl(xeConstraint.getAttribute("config"), "");
                    switch (xeConstraint.getAttribute("name").toUpperCase()) {
                      case "REQUIRED":
                        validatorsObj.push(Validators.required);                   
                        break;
                      case "MIN":
                        if (validationVal !== "") {
                          validatorsObj.push(Validators.min(Number(validationVal))); 
                        }
                        break;
                      case "MINLENGTH":
                        if (validationVal !== "") 
                          validatorsObj.push(Validators.minLength(Number(validationVal)));
                        break;
                      case "MAX":
                        if (validationVal !== "") {
                          validatorsObj.push(Validators.max(Number(validationVal))); 
                        }
                        break;
                      case "MAXLENGTH":
                        if (validationVal !== "") 
                          validatorsObj.push(Validators.maxLength(Number(validationVal)));  
                        break;
                      case "READONLY":
                        this.form.controls[fieldInfo.name].disable();
                        break;
                    }
                  });
    
                  if (validatorsObj.length > 0) {
                    this.form.controls[fieldInfo.name].setValidators(validatorsObj);
                    this.form.controls[fieldInfo.name].updateValueAndValidity();
                  }
                }
              });
            }

          
          });
          console.log(JSON.stringify(this.fieldsList));
        },
        error => {
          this.loaded = false;
          console.log('getProcessXmlError ', error);
        }
      );

    // this.loaded = true;
    // this.opemTasksSubscription = this.bpmnService
    //   .getFormVariables(this.currentTaskId)
    //   .subscribe(
    //     (result) => {
    //       this.fieldsList = [];
    //       Object.keys(result).forEach((key) => {
    //         let item = result[key];
    //         let fieldInfo: TaskFieldInfo = new TaskFieldInfo();
    //         fieldInfo.name = key;
    //         fieldInfo.type = item.type;
    //         fieldInfo.value = item.value;
    //         this.fieldsList.push(fieldInfo);
    //         this.form.addControl(
    //           key,
    //           new FormControl(item.value, [Validators.required])
    //         );
    //       });
    //       this.loaded = false;
    //     },
    //     (error) => {
    //       console.log("openTaskError ", error);
    //       this.loaded = false;
    //     }
    //   );
  }

  sendForm() {
    this.loaded = true;
    let vars = "{";
    if (this.fieldsList.length > 0) {
      vars += "\"variables\": {";
      for (var idx = 0; idx < this.fieldsList.length; idx++) {
        vars += "\"" + this.fieldsList[idx].name + "\": {\"value\":";
        switch(this.fieldsList[idx].type.toUpperCase()) {
          case "LONG":
          case "BOOLEAN":
            vars += this.form.get(this.fieldsList[idx].name).value;
            break;
          case "DATE":
            vars += "\"" + this.toDateCamunda(this.form.get(this.fieldsList[idx].name).value) + "\""; 
            break; 
          default:
            vars += "\"" + this.form.get(this.fieldsList[idx].name).value + "\"";
            break;
        }
         vars += ", \"type\": \"" + "String"/*this.fieldsList[idx].type*/ + "\"}";

        if (idx != this.fieldsList.length - 1) vars += ", ";
      }
      vars += "}";
    }
    vars += "}";

    console.log(vars);

    let varsObj = JSON.parse(vars);

    this.sendFormSubscription = this.bpmnService
      .submitTaskForm(this.currentTaskId, varsObj)
      .subscribe(
        (result) => {
          this.loaded = false;
          this.fromTaskForm.emit(this.currentTaskId);
        },
        (error) => {
          console.log("submitError ", error);
          this.loaded = false;
        }
      );
  }

  closeForm() {
    this.fromTaskForm.emit("");
  }
}
