import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { TastListComponent } from 'src/app/modules/tast-list/tast-list.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { MatSidenavModule, 
    MatDividerModule, 
    MatCardModule, 
    MatPaginatorModule, 
    MatTableModule, 
    MatExpansionModule, 
    MatButtonModule,
    MatDialogModule,
    MatCheckboxModule,
    MatInputModule,
    MatFormFieldModule,
    MatTabsModule,
    MatSelectModule,
    MatIconModule,
    MatMenuModule,
    MatDatepickerModule,
    MatProgressSpinnerModule} from '@angular/material';

import { BpmnService } from 'src/app/services/bpmn.service';

import { TabComponent } from 'src/app/shared/components/tabs/tab.component';
import { TabsComponent } from 'src/app/shared/components/tabs/tabs.component';

import { StartProcessFormComponent } from 'src/app/modules/start-process-form/start-process-form.component';
import { TaskItemComponent } from 'src/app/modules/task-item/task-item.component';
import { DiagramComponent } from 'src/app/dialogs/diagram/diagram.component';

import { DoubleClickDirective } from 'src/app/shared/directives/double-click.directive';
import { TaskTabDirective } from 'src/app/shared/directives/task-tab.directive';
import { ApprovalHierarchyComponent } from 'src/app/modules/approval-hierarchy/approval-hierarchy.component';

@NgModule({
  declarations: [
    DoubleClickDirective,
    DefaultComponent,
    TastListComponent,
    TabComponent, 
    TabsComponent,
    TaskItemComponent,
    StartProcessFormComponent,
    DiagramComponent,
    ApprovalHierarchyComponent,
    TaskTabDirective
  ],
  entryComponents: [
    DiagramComponent
  ],
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    FlexLayoutModule,
    MatCardModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTabsModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    
    CKEditorModule
  ],
  providers: [
    BpmnService
  ]
})
export class DefaultModule { }
