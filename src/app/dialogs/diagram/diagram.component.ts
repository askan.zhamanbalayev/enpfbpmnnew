import { Component, 
  OnInit, 
  Inject,
  OnDestroy,
  AfterViewInit,
  ElementRef,
  ViewChild
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { BpmnService } from '../../services/bpmn.service';
import * as BpmnJS from '../../bpmn-js/dist/bpmn-navigated-viewer.development.js';
import * as $ from 'jquery';

export interface DialogData {
  processDefinitionId: string;
  taskDefinitionKey: string;
  taskName: string;
}

@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.scss']
})
export class DiagramComponent implements OnInit, AfterViewInit, OnDestroy {
  
  @ViewChild('ref', {static: false}) el: ElementRef;
  @ViewChild('scrollMe', {static: false}) scrollMe: ElementRef;
  getParamsSubscription: Subscription;
  getProcessDiagramSubscription: Subscription;
  bpmnJS: BpmnJS;
  loaded: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, 
    public dialogRef: MatDialogRef<DiagramComponent>, 
    public bpmnService:BpmnService) 
  {
    this.bpmnJS = new BpmnJS();
    this.bpmnJS.on('import.done', ({ error }) => {
      if (!error) {
        this.bpmnJS.get('canvas').zoom('fit-viewport', 'auto');
      }
    });
  }
  
  ngOnInit() {
    this.getProcessDiagram();
  }

  ngAfterViewInit() {
    this.bpmnJS.attachTo(this.el.nativeElement);
  }

  ngOnDestroy() {
    if (this.getParamsSubscription) {
      this.getParamsSubscription.unsubscribe();
    }

    if (this.getProcessDiagramSubscription) {
      this.getProcessDiagramSubscription.unsubscribe();
    }

    if (this.bpmnJS) {
      this.bpmnJS.destroy();
    }
  }

  getProcessDiagram() {
    this.loaded = true;
    let processDefId = this.data.processDefinitionId;
    let taskDefinitionKey = this.data.taskDefinitionKey;

    this.getProcessDiagramSubscription = this.bpmnService
      .getProcessXmlByDefinitionId(processDefId)
      .subscribe(
        result => {
          this.bpmnJS.importXML(result.bpmn20Xml, () => {            
            this.loaded = false; 
            if (taskDefinitionKey !== "") {
              let overlays = this.bpmnJS.get('overlays');
              let elementRegistry = this.bpmnJS.get('elementRegistry');
              var shape = elementRegistry.get(taskDefinitionKey);
              let $overlayHtml = $(
                `<div style="background-color:#15b427;
                            opacity:.5;
                            border-radius:13px;
                            position:absolute;
                            top:5px;
                            left:5px;
                            pointer-events:none">`)
                .css({
                  width: shape.width,
                  height: shape.height
                });
              overlays.add(taskDefinitionKey, {
                position: {
                  top: -5,
                  left: -5
                },
                html: $overlayHtml
              }); 
            }
            
          });
        },
        error => {
          console.log('getprocessDiagramError = ', error);
          this.loaded = false;
        },
        () => {
          
          
          setTimeout(() => {
            console.log('fsdfdsfdsfs');
            var height = this.scrollMe.nativeElement.offsetHeight;
          var scrollTop = this.scrollMe.nativeElement.scrollTop;  
          var scrollHeight = this.scrollMe.nativeElement.scrollHeight
          var scroll = scrollHeight - scrollTop == height;
            if(scroll)
            this.scrollTop = this.scrollMe.nativeElement.scrollHeight;
          }, 2000);
          
        }
    );
  }

  scrollTop = 200;

  onDiagramClose(): void {
    this.dialogRef.close();
  }
}
