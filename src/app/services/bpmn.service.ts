import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProcessDefinition, TaskInfo } from '../shared/data/bpmn';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BpmnService {
  //public baseUrl = 'https://enpfbpmn.enpf.kz/engine-rest';
  //public baseUrl = 'http://kubtest02.enpf.kz:8080/engine-rest';
  public baseUrl = 'http://localhost:8080/engine-rest';

  constructor(private http: HttpClient) { }

  getAllProcessDefinitions(): Observable<ProcessDefinition[]> {
    return (this.http.get(`${this.baseUrl}/process-definition`) as Observable<ProcessDefinition[]>);
  }
  
  deployDiagram(file: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/deployment/create`, file);
  }

  deployDiagramByXml(xml: any) {
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'text/plain'
    }); 
    return this.http.post(`${this.baseUrl}/deployment/create`, xml, { headers: httpHeaders });
  }

  

  startProcessById(id: string, data: string) {
    let httpHeaders = new HttpHeaders({
      'Content-Type': ' application/json'
    });    
    return this.http.post(`${this.baseUrl}/process-definition/${id}/start`, data, { headers: httpHeaders });
  }

  getProcessFormVariables(processDefinitionId) : Observable<any> {
    return this.http.get(`${this.baseUrl}/process-definition/${processDefinitionId}/form-variables`);
  }

  getProcessXmlByDefinitionId(processDefinitionId): Observable<any> {
    return this.http.get(`${this.baseUrl}/process-definition/${processDefinitionId}/xml`);
  }

  getTasksByAssignee(assignee:any): Observable<TaskInfo[]> {
    return (this.http.get(`${this.baseUrl}/task?assignee=${assignee}`) as Observable<TaskInfo[]>);
  }

  getAllTasks(): Observable<TaskInfo[]> {
    return (this.http.get(`${this.baseUrl}/task`) as Observable<TaskInfo[]>);
  }

  getFormVariables(taskId: any): Observable<any> {
    return this.http.get(`${this.baseUrl}/task/${taskId}/form-variables`);
  }

  submitTaskForm(taskId: any, variables: any) {
    let httpHeaders = new HttpHeaders({
      'Content-Type': ' application/json'
    }); 
    return this.http.post(`${this.baseUrl}/task/${taskId}/submit-form`, variables, { headers: httpHeaders });
  }

}
