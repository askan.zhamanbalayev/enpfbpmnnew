import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpEvent
} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class AuthInterceptor implements HttpInterceptor{
  constructor(){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      withCredentials: true
      // setHeaders: { 
      //   Authorization: 'Basic V2ViQ9C10YDQstC40YE6MzI1NUB4ZGxwQA=='
      // }
    });
    return next.handle(req);
  }
}
