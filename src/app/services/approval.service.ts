import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApprovalService {
  public baseUrl = 'http://localhost:9090';

  constructor(private http: HttpClient) { }

  getAllApproval(): Observable<any> {
    return (this.http.get(`${this.baseUrl}/hierarchy/findAll`));
  }

  getAllApprovalByProcesskey(processKey: any): Observable<any> {
    return (this.http.get(`${this.baseUrl}/hierarchy/findByProcessKey/${processKey}`));
  }
  
  createApproval(approval: any): Observable<any> {
    let httpHeaders = new HttpHeaders({
        'Content-Type': ' application/json'
    }); 
    return this.http.post(`${this.baseUrl}/hierarchy/create`, approval, { headers: httpHeaders });
  }

  editApproval(approvalId: any, approval: any): Observable<any> {
    let httpHeaders = new HttpHeaders({
        'Content-Type': ' application/json'
    }); 
    return this.http.post(`${this.baseUrl}/hierarchy/update/${approvalId}`, approval, { headers: httpHeaders });
  }

  deleteApproval(approvalId: any): Observable<any> {
    return (this.http.get(`${this.baseUrl}/hierarchy/delete/${approvalId}`)); 
  }

  getAllOwners(): Observable<any> {
    return (this.http.get(`${this.baseUrl}/owners/findAll`));
  }

  getAllOwnersByProcesskey(processKey: any): Observable<any> {
    return (this.http.get(`${this.baseUrl}/owners/findByProcessKey/${processKey}`));
  }

  getAllOwnersByOwnerId(ownerId: any) {
    return (this.http.get(`${this.baseUrl}/owners/findByOwnerUser/${ownerId}`));
  }

  getAllOwnersByDepartmentId(departmentId: any): Observable<any> {
    return (this.http.get(`${this.baseUrl}/owners/findByOwnerDepartment/${departmentId}`));
  }

  createOwner(owner: any): Observable<any> {
    let httpHeaders = new HttpHeaders({
        'Content-Type': ' application/json'
    }); 
    return this.http.post(`${this.baseUrl}/owners/create`, owner, { headers: httpHeaders });
  }

  editOwner(ownerId: any, owner: any): Observable<any> {
    let httpHeaders = new HttpHeaders({
        'Content-Type': ' application/json'
    }); 
    return this.http.post(`${this.baseUrl}/owners/update/${ownerId}`, owner, { headers: httpHeaders });
  }

  deleteOwner(ownerId: any): Observable<any> {
    return (this.http.get(`${this.baseUrl}/owners/delete/${ownerId}`)); 
  }

  getNextApporval(processKey, departmentIds, userIds) {
    return (this.http.get(`${this.baseUrl}/getNextApproval/processKey=${processKey}/departmentIds=${departmentIds}/userIds=${userIds}`)); 
  }
}
