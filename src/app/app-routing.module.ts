import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layouts/default/default.component';
import { TastListComponent } from './modules/tast-list/tast-list.component';
import { ApprovalHierarchyComponent } from './modules/approval-hierarchy/approval-hierarchy.component';

const routes: Routes = [{
  path: '',
  component: DefaultComponent,
  children: [{
    path: '',
    component: TastListComponent
  },
  {
    path: 'task-list',
    component: TastListComponent 
  },
  {
    path: 'approval-hierarchy',
    component: ApprovalHierarchyComponent 
  },
  {
    path: '',
    redirectTo: 'approval-hierarchy',
    pathMatch: 'full',
  }
]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
